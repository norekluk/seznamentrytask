# Search book by author

Example app written in Kotlin using Jetpack Compose that showcases my level of nderstanding of these technologies.


## Run Locally

Clone the project

```bash
git clone https://gitlab.com/norekluk/searchbooksbyauthor.git
```

Create Google Book API Key

```bash
Follow these instructions on how to create API Key.
https://developers.google.com/books/docs/v1/using#APIKey
```
Go to the project directory

```bash
cd searchbooksbyauthor
```

Create local.properties file
```bash
touch local.properties
```

Put this line inside the local.properties file. Put your generated API key instead of 'your_api_key'
```bash
GOOGLE_BOOKS_API_KEY=your_api_key
```

Run the application

```bash
./gradlew run
```
Alternative: Run the application from Android Studio

```bash
If you open this project in Android studio you can run it directly by 
using top menu 'Run > Run 'app'.
```
