package com.example.searchbooksbyauthor.service

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.searchbooksbyauthor.model.data.Volume
import com.example.searchbooksbyauthor.nextKey
import com.example.searchbooksbyauthor.prevKey

class VolumesPagingSource(private val query: String, private val repository: VolumesRepository) :
    PagingSource<Int, Volume>() {
    override fun getRefreshKey(state: PagingState<Int, Volume>): Int {
        return ((state.anchorPosition ?: 0) - state.config.initialLoadSize / 2).coerceAtLeast(0)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Volume> {
        return try {
            val nextPageNumber = params.key ?: 1
            val response = repository.getVolumesByAuthor(query, nextPageNumber, params.loadSize)

            LoadResult.Page(
                data = response.first,
                prevKey = params.prevKey(),
                nextKey = params.nextKey(response.second ?: 0)
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}
