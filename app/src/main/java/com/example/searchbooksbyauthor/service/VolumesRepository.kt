package com.example.searchbooksbyauthor.service

import android.util.Log
import com.example.searchbooksbyauthor.model.data.Volume

class VolumesRepository {
    private val volumesService = RetrofitInstance.volumesService

    private val allVolumes = mutableSetOf<Volume>()

    suspend fun getVolumesByAuthor(author: String, startIndex: Int, maxResults: Int): Pair<List<Volume>, Int?> {
        Log.d(Class::class.simpleName, "Author: $author")
        val result = volumesService.getVolumesByAuthor("inauthor:$author", startIndex, maxResults, "cs")

        // eliminate duplicates from response and remove already received items
        val resultItemsSet = result.items.toSet() subtract allVolumes
        allVolumes.addAll(resultItemsSet)
        val resultTotalItems = result.totalItems

        return resultItemsSet.toList() to resultTotalItems
    }

    fun getVolumeByTitle(id: String): Volume? {
        return allVolumes.find { it.id == id }
    }

    fun clear() {
        allVolumes.clear()
    }
}