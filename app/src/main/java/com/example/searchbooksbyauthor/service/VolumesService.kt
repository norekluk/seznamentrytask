package com.example.searchbooksbyauthor.service

import com.example.searchbooksbyauthor.model.data.Volume
import com.example.searchbooksbyauthor.model.data.Volumes
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface VolumesService {
    @GET("volumes")
    suspend fun getVolumesByAuthor(
        @Query("q") author: String,
        @Query("startIndex") startIndex: Int,
        @Query("maxResults") maxResults: Int,
        @Query("langRestrict") langRestrict: String
    ): Volumes

    @GET("volumes/{id}")
    suspend fun getVolumeById(
        @Path("id") id: String
    ): Volume
}