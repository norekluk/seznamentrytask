package com.example.searchbooksbyauthor.ui.theme.overview

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.items
import com.example.searchbooksbyauthor.R
import com.example.searchbooksbyauthor.model.data.Volume

@Composable
fun OverviewContent(
    searchText: String,
    volumes: LazyPagingItems<Volume>,
    listState: LazyListState,
    onItemClick: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    if (searchText.isEmpty()) {
        OverviewBigText(
            text = stringResource(R.string.search_empty_prompt),
            modifier = modifier
        )
    } else {
        when (volumes.loadState.refresh) {
            LoadState.Loading -> {
                Box(modifier = Modifier.fillMaxSize()) {
                    CircularProgressIndicator(
                        Modifier
                            .align(Alignment.Center)
                            .testTag(stringResource(R.string.loading_test_tag))
                    )
                }
            }

            is LoadState.Error -> {
                OverviewBigText(
                    text = stringResource(R.string.loading_error),
                    modifier = modifier
                )
            }

            else -> {
                if (volumes.itemCount == 0) {
                    OverviewBigText(
                        text = "No results for author: $searchText",
                        modifier = modifier
                    )
                }
                LazyColumn(
                    state = listState,
                    modifier = modifier,
                    verticalArrangement = Arrangement.spacedBy(8.dp),
                    contentPadding = PaddingValues(vertical = 16.dp, horizontal = 16.dp),
                ) {
                    items(volumes, key = { it.id!! }) { volume ->
                        BookItem(
                            name = volume?.volumeInfo?.title,
                            authors = volume?.volumeInfo?.authors,
                            thumbnail = volume?.volumeInfo?.imageLinks?.thumbnail,
                            onClick = {
                                onItemClick(volume?.id!!)
                            },
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
        }
    }
}