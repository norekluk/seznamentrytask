package com.example.searchbooksbyauthor.ui.theme.overview

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.searchbooksbyauthor.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OverviewSearchBar(
    isSearching: Boolean,
    searchText: String,
    onSearchTextChange: (String) -> Unit,
    onSearch: (String) -> Unit,
    onActiveChange: (Boolean) -> Unit,
    onTrailingIconClick: () -> Unit
) {
    SearchBar(
        modifier = Modifier
            .padding(horizontal = 16.dp)
            .padding(top = 8.dp)
            .fillMaxWidth(),
        query = searchText,
        onQueryChange = onSearchTextChange,
        onSearch = onSearch,
        active = isSearching,
        onActiveChange = onActiveChange,
        leadingIcon = {
            Icon(
                imageVector = Icons.Default.Search,
                contentDescription = stringResource(R.string.content_description_search_icon)
            )
        },
        trailingIcon = {
            IconButton(onClick = onTrailingIconClick) {
                Icon(
                    imageVector = Icons.Default.Close,
                    contentDescription = stringResource(R.string.content_description_delete_search_text),
                )
            }
        },
        placeholder = {
            Text(text = stringResource(R.string.search_placeholder))
        }
    ) {}
}