package com.example.searchbooksbyauthor.ui.theme.detail

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.searchbooksbyauthor.R
import com.example.searchbooksbyauthor.ui.theme.SearchBooksByAuthorTheme

@Composable
fun BookDetailContent(
    name: String,
    authors: String,
    bookDescription: String?,
    thumbnail: String?,
    releaseDate: String?,
    modifier: Modifier = Modifier,
) {
    Surface(modifier = modifier.padding(horizontal = 16.dp)) {
        Column {
            Row(verticalAlignment = Alignment.CenterVertically) {
                val resId = R.drawable.book_placeholder
                AsyncImage(
                    model = thumbnail,
                    contentDescription = stringResource(id = R.string.content_description_book_thumbnail),
                    placeholder = painterResource(resId),
                    error = painterResource(resId),
                    contentScale = ContentScale.FillWidth,
                    modifier = Modifier.width(80.dp).testTag(thumbnail ?: resId.toString()),
                )
                Column(modifier = Modifier.padding(horizontal = 16.dp)) {
                    Text(text = name, style = MaterialTheme.typography.headlineLarge)
                    Text(text = authors, style = MaterialTheme.typography.titleLarge)
                    Text(
                        text = releaseDate ?: stringResource(R.string.release_date_placeholder),
                        style = MaterialTheme.typography.labelLarge
                    )
                }
            }
            Spacer(modifier = Modifier.size(16.dp))
            Text(
                text = bookDescription ?: stringResource(R.string.no_description_placeholder), modifier = Modifier.verticalScroll(
                    rememberScrollState()
                )
            )
        }
    }
}

@Preview
@Composable
fun BookDetailPreview() {
    SearchBooksByAuthorTheme {
        BookDetailContent(
            name = "I, Robot",
            authors = "Isaac Asimov",
            bookDescription = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam quis nulla. Sed ac dolor sit amet purus malesuada congue. Nulla non arcu lacinia neque faucibus fringilla. Nam sed tellus id magna elementum tincidunt. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Vivamus luctus egestas leo. Maecenas libero. Fusce aliquam vestibulum ipsum. ",
            thumbnail = "http://books.google.com/books/content?id=_ojXNuzgHRcC&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE73ofWeWPkb4BIqYBiZvFA8kMLiSF2BkOMfHtjJLEulQnah3QKpVpwcYxjydjAYEhlhBzTyc_7jr8_varWz8ftBqbQj4KkVEWdtVB-skF_Xruow-_hONQfI2LJp0YdFo4gDUY7X8&source=gbs_api",
            releaseDate = "1939"
        )
    }
}