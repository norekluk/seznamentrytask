package com.example.searchbooksbyauthor.ui.theme.detail


import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Shop
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.searchbooksbyauthor.R
import com.example.searchbooksbyauthor.makeValid
import com.example.searchbooksbyauthor.navigation.BackPressHandler
import com.example.searchbooksbyauthor.ui.theme.SearchBooksByAuthorTheme
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BookDetailPage(
    authors: ArrayList<String>?,
    bookName: String,
    bookDescription: String?,
    thumbnail: String?,
    releaseDate: String?,
    storeUrl: String?,
    onBackButtonClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val uriHandler = LocalUriHandler.current
    val scope = rememberCoroutineScope()
    val snackBarHostState = remember {
        SnackbarHostState()
    }
    val noStoreUrl = stringResource(R.string.no_store_url)
    BackPressHandler(onBackPressed = onBackButtonClick)
    Scaffold(
        modifier = modifier,
        snackbarHost = {
            SnackbarHost(hostState = snackBarHostState)
        },
        topBar = {
            TopAppBar(
                title = {
                },
                navigationIcon = {
                    IconButton(
                        onClick = onBackButtonClick
                    ) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(R.string.back_button)
                        )
                    }
                },
                actions = {
                    IconButton(
                        onClick = {
                            if (storeUrl != null) {
                                uriHandler.openUri(storeUrl)
                            } else {
                                scope.launch {
                                    snackBarHostState.showSnackbar(
                                        noStoreUrl
                                    )
                                }
                            }
                        }) {
                        Icon(
                            imageVector = Icons.Default.Shop,
                            contentDescription = stringResource(R.string.google_play_link)
                        )
                    }
                }

            )
        }
    ) { innerPadding ->
        BookDetailContent(
            name = bookName,
            authors = authors.makeValid(),
            bookDescription = bookDescription,
            thumbnail = thumbnail,
            releaseDate = releaseDate,
            modifier = Modifier.padding(innerPadding)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun BookDetailPagePreview() {
    SearchBooksByAuthorTheme {
        BookDetailPage(
            authors = arrayListOf("Isaac Asimov"),
            bookName = "I, Robot",
            bookDescription = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam quis nulla. Sed ac dolor sit amet purus malesuada congue. Nulla non arcu lacinia neque faucibus fringilla. Nam sed tellus id magna elementum tincidunt. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Vivamus luctus egestas leo. Maecenas libero. Fusce aliquam vestibulum ipsum. ",
            thumbnail = "http://books.google.com/books/content?id=_ojXNuzgHRcC&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE73ofWeWPkb4BIqYBiZvFA8kMLiSF2BkOMfHtjJLEulQnah3QKpVpwcYxjydjAYEhlhBzTyc_7jr8_varWz8ftBqbQj4KkVEWdtVB-skF_Xruow-_hONQfI2LJp0YdFo4gDUY7X8&source=gbs_api",
            releaseDate = "1939",
            storeUrl = "http://books.google.cz/books?id=OSk9zwEACAAJ&dq=inauthor:asimov&hl=&source=gbs_api",
            onBackButtonClick = {}
        )
    }
}