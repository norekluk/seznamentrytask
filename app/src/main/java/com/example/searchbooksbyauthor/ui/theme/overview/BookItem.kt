package com.example.searchbooksbyauthor.ui.theme.overview

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.searchbooksbyauthor.R
import com.example.searchbooksbyauthor.makeValid
import com.example.searchbooksbyauthor.ui.theme.SearchBooksByAuthorTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BookItem(
    name: String?,
    authors: ArrayList<String>?,
    thumbnail: String?,
    modifier: Modifier = Modifier,
    onClick: () -> Unit = {},
) {
    Card(
        onClick = onClick,
        modifier = modifier
    ) {
        Column {
            Row(verticalAlignment = Alignment.CenterVertically) {
                AsyncImage(
                    model = thumbnail,
                    contentDescription = stringResource(R.string.content_description_book_thumbnail),
                    placeholder = painterResource(id = R.drawable.book_placeholder),
                    error = painterResource(id = R.drawable.book_placeholder),
                    contentScale = ContentScale.FillWidth,
                    modifier = Modifier.width(80.dp)
                )
                Column(modifier = Modifier.padding(horizontal = 16.dp)) {
                    Text(text = name ?: stringResource(R.string.book_title_placeholder), style = MaterialTheme.typography.headlineSmall)

                    Text(text = authors.makeValid(), style = MaterialTheme.typography.labelLarge)
                }
            }
        }
    }
}

@Preview
@Composable
fun BookItemPreview() {
    SearchBooksByAuthorTheme {
        BookItem(name = "I, Robot", authors = arrayListOf("Isaac Asimov"), thumbnail = "")
    }
}



