package com.example.searchbooksbyauthor.ui.theme.overview

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.paging.compose.collectAsLazyPagingItems
import com.example.searchbooksbyauthor.model.VolumesViewModel
import com.example.searchbooksbyauthor.ui.theme.SearchBooksByAuthorTheme
import kotlinx.coroutines.launch


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Overview(volumesViewModel: VolumesViewModel, onItemClick: (String) -> Unit = {}) {
    val isSearching by volumesViewModel.isSearching.collectAsState()
    val volumes = volumesViewModel.volumesPager.collectAsLazyPagingItems()
    val searchText by volumesViewModel.searchText.collectAsState()
    val listState = rememberLazyListState()
    val fabVisible by remember {
        derivedStateOf {
            listState.firstVisibleItemIndex > 0 && searchText.isNotEmpty()
        }
    }
    val scope = rememberCoroutineScope()
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            OverviewSearchBar(
                isSearching = isSearching,
                searchText = searchText,
                onSearchTextChange = volumesViewModel::onSearchTextChange,
                onSearch = {
                    scope.launch { listState.scrollToItem(0) }
                    volumesViewModel.onSearch()
                },
                onActiveChange = {
                    volumesViewModel.onToggleSearch()
                },
                onTrailingIconClick = {
                    scope.launch { listState.scrollToItem(0) }
                    volumesViewModel.clearSearchText()
                }
            )
        },
        floatingActionButton = {
            AnimatedVisibility(
                visible = fabVisible,
                enter = fadeIn(),
                exit = fadeOut()
            ) {
                FloatingActionButton(onClick = { scope.launch { listState.animateScrollToItem(0) } }) {
                    Icon(Icons.Default.KeyboardArrowUp, "Scroll to top")
                }
            }
        }
    ) {
        OverviewContent(
            searchText = searchText,
            volumes = volumes,
            listState = listState,
            onItemClick = onItemClick,
            modifier = Modifier.padding(it)
        )
    }
}

@Preview
@Composable
fun OverviewPreview() {
    SearchBooksByAuthorTheme {
        Overview(VolumesViewModel())
    }
}
