package com.example.searchbooksbyauthor

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.example.searchbooksbyauthor.model.VolumesViewModel
import com.example.searchbooksbyauthor.navigation.SeznamEntryTaskNavHost
import com.example.searchbooksbyauthor.ui.theme.SearchBooksByAuthorTheme

class MainActivity : ComponentActivity() {
    private val viewModel: VolumesViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainApp(viewModel)
        }
    }
}


@Composable
fun MainApp(viewModel: VolumesViewModel) {
    SearchBooksByAuthorTheme {
        // A surface container using the 'background' color from the theme
        val navHostController = rememberNavController()
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            SeznamEntryTaskNavHost(navController = navHostController, viewModel = viewModel)
        }
    }
}