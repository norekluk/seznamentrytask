package com.example.searchbooksbyauthor.model.data

import com.google.gson.annotations.SerializedName

data class Dimensions(
    @SerializedName("height") var height: String? = null,
    @SerializedName("width") var width: String? = null,
    @SerializedName("thickness") var thickness: String? = null
)
