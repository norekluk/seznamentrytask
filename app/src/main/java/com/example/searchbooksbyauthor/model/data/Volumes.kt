package com.example.searchbooksbyauthor.model.data

import com.google.gson.annotations.SerializedName

data class Volumes(
    @SerializedName("kind"       ) var kind       : String?          = null,
    @SerializedName("totalItems" ) var totalItems : Int?             = null,
    @SerializedName("items"      ) var items      : ArrayList<Volume> = arrayListOf()
)