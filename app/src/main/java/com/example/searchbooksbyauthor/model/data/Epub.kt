package com.example.searchbooksbyauthor.model.data

import com.google.gson.annotations.SerializedName

data class Epub(
    @SerializedName("isAvailable" ) var isAvailable : Boolean? = null
)
