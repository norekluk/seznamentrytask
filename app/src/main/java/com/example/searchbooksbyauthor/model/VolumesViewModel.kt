package com.example.searchbooksbyauthor.model

import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.searchbooksbyauthor.model.data.Volume
import com.example.searchbooksbyauthor.service.VolumesPagingSource
import com.example.searchbooksbyauthor.service.VolumesRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

const val PAGE_SIZE = 10

class VolumesViewModel : ViewModel() {

    private val repository = VolumesRepository()
    private lateinit var pagingSource: VolumesPagingSource

    val volumesPager: Flow<PagingData<Volume>> = Pager(PagingConfig(PAGE_SIZE)) {
        VolumesPagingSource(searchText.value, repository).also { pagingSource = it }
    }.flow

    private val _isSearching = MutableStateFlow(false)
    val isSearching = _isSearching.asStateFlow()

    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    fun onSearchTextChange(text: String) {
        _searchText.value = text
    }

    fun onToggleSearch() {
        _isSearching.value = !_isSearching.value
    }

    fun onSearch() {
        _isSearching.value = !_isSearching.value
        repository.clear()
        pagingSource.invalidate()
    }

    fun clearSearchText() {
        _searchText.value = ""
    }

    fun getItem(id: String): Volume? {
        return repository.getVolumeByTitle(id = id)
    }

    fun clearRepositoryCache() {
        repository.clear()
    }
}