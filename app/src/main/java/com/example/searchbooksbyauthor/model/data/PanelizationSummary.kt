package com.example.searchbooksbyauthor.model.data

import com.google.gson.annotations.SerializedName

data class PanelizationSummary(
    @SerializedName("containsEpubBubbles") var containsEpubBubbles: Boolean? = null,
    @SerializedName("containsImageBubbles") var containsImageBubbles: Boolean? = null
)
