package com.example.searchbooksbyauthor.model.data

import com.google.gson.annotations.SerializedName

data class Volume(
    @SerializedName("kind"       ) var kind       : String?     = null,
    @SerializedName("id"         ) var id         : String?     = null,
    @SerializedName("etag"       ) var etag       : String?     = null,
    @SerializedName("selfLink"   ) var selfLink   : String?     = null,
    @SerializedName("volumeInfo" ) var volumeInfo : VolumeInfo = VolumeInfo(),
    @SerializedName("saleInfo"   ) var saleInfo   : SaleInfo?   = SaleInfo(),
    @SerializedName("accessInfo" ) var accessInfo : AccessInfo? = AccessInfo()
) {
    override fun hashCode(): Int {
        return 31 * this.volumeInfo.title.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Volume) return false

        return this.volumeInfo.title == other.volumeInfo.title
    }
}