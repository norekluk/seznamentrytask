package com.example.searchbooksbyauthor

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import androidx.paging.PagingSource
import com.example.searchbooksbyauthor.navigation.BookDetailPage

fun NavHostController.navigateToBookDetailPage(bookId: String) {
    this.navigate("${BookDetailPage.route}/$bookId")
}

@Composable
fun ArrayList<String>?.makeValid(): String {
    return this?.let {
        if (it.isEmpty()) {
            stringResource(R.string.authors_placeholder)
        } else {
            it.joinToString(separator = ", ")
        }
    } ?: stringResource(R.string.authors_placeholder)
}

fun PagingSource.LoadParams<Int>.prevKey() =
    (key?.coerceAtLeast(0) ?: 0).takeIf { it > 0 }?.minus(loadSize)?.coerceAtLeast(0)

fun PagingSource.LoadParams<Int>.nextKey(total: Int) =
    (key?.coerceAtLeast(0) ?: 0).plus(loadSize).takeIf { it <= total }