package com.example.searchbooksbyauthor.navigation

import androidx.navigation.NavType
import androidx.navigation.navArgument

interface SeznamEntryTaskDestinations {
    val route: String
}

object Overview: SeznamEntryTaskDestinations {
    override val route: String = "overview"
}

object BookDetailPage: SeznamEntryTaskDestinations {
    override val route: String = "book_detail"
    const val bookIdArg = "book_id"
    val routeWithArgs = "$route/{$bookIdArg}"
    val arguments = listOf(
        navArgument(bookIdArg) { type = NavType.StringType},
    )
}