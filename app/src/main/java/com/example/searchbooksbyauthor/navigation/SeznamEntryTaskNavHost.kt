package com.example.searchbooksbyauthor.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.searchbooksbyauthor.model.VolumesViewModel
import com.example.searchbooksbyauthor.navigateToBookDetailPage
import com.example.searchbooksbyauthor.ui.theme.overview.Overview


@Composable
fun SeznamEntryTaskNavHost(
    navController: NavHostController,
    viewModel: VolumesViewModel,
    modifier: Modifier = Modifier
) {
    NavHost(
        navController = navController,
        startDestination = Overview.route,
        modifier = modifier
    ) {
        composable(route = Overview.route) {
            Overview(volumesViewModel = viewModel, onItemClick = { bookId ->
                navController.navigateToBookDetailPage(bookId = bookId)
            })
        }
        composable(
            route = BookDetailPage.routeWithArgs,
            arguments = BookDetailPage.arguments
        ) {

            val id = it.arguments?.getString(BookDetailPage.bookIdArg)!!
            val volume = viewModel.getItem(id)

            volume?.volumeInfo?.let { volumeInfo ->
                com.example.searchbooksbyauthor.ui.theme.detail.BookDetailPage(
                    authors = volumeInfo.authors,
                    bookName = volumeInfo.title!!,
                    bookDescription = volumeInfo.description,
                    thumbnail = volumeInfo.imageLinks?.thumbnail,
                    releaseDate = volumeInfo.publishedDate,
                    storeUrl = volumeInfo.infoLink,
                    onBackButtonClick = {
                        viewModel.clearRepositoryCache()
                        navController.navigateUp()
                    })
            }
        }
    }
}