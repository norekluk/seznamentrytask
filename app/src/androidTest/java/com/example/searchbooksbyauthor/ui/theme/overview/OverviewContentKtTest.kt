package com.example.searchbooksbyauthor.ui.theme.overview

import androidx.activity.ComponentActivity
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.paging.PagingData
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import com.example.searchbooksbyauthor.R
import com.example.searchbooksbyauthor.model.data.Volume
import com.example.searchbooksbyauthor.model.data.VolumeInfo
import kotlinx.coroutines.flow.MutableStateFlow
import org.junit.Rule
import org.junit.Test

class OverviewContentTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Test
    fun testOverviewContent() {
        val activity = composeTestRule.activity
        val searchText = "Isaac Asimov"
        var volumes: LazyPagingItems<Volume>
        val listState = LazyListState()

        composeTestRule.setContent {
            volumes = createDummyVolumes().collectAsLazyPagingItems()
            OverviewContent(
                searchText = searchText,
                volumes = volumes,
                listState = listState,
                onItemClick = {},
                modifier = Modifier
            )
        }

        composeTestRule.onNodeWithTag(activity.getString(R.string.loading_test_tag)).assertExists()

    }

    private fun createDummyVolumes(): MutableStateFlow<PagingData<Volume>> {
        val volumes = listOf(
            Volume(id = "1", volumeInfo = VolumeInfo(title = "Book 1")),
            Volume(id = "2", volumeInfo = VolumeInfo(title = "Book 2"))
        )
// create pagingData from a list of fake data
        val pagingData = PagingData.from(volumes)
// pass pagingData containing fake data to a MutableStateFlow
        val fakeDataFlow = MutableStateFlow(pagingData)
        return fakeDataFlow
    }

}