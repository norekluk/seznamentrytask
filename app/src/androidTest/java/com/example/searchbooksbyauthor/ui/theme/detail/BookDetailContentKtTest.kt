package com.example.searchbooksbyauthor.ui.theme.detail

import androidx.activity.ComponentActivity
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsNotDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import com.example.searchbooksbyauthor.R
import org.junit.Rule
import org.junit.Test

class BookDetailContentTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Test
    fun testBookDetailContent() {
        val thumbnailString = "http://books.google.com/books/content?id=_ojXNuzgHRcC&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE73ofWeWPkb4BIqYBiZvFA8kMLiSF2BkOMfHtjJLEulQnah3QKpVpwcYxjydjAYEhlhBzTyc_7jr8_varWz8ftBqbQj4KkVEWdtVB-skF_Xruow-_hONQfI2LJp0YdFo4gDUY7X8&source=gbs_api"
        val bookDescription = "Lorem ipsum dolor sit amet"
        composeTestRule.setContent {
            BookDetailContent(
                name = "I, Robot",
                authors = "Isaac Asimov",
                bookDescription = bookDescription,
                thumbnail = thumbnailString,
                releaseDate = "1939"
            )
        }

        composeTestRule.onNodeWithText("I, Robot").assertIsDisplayed()
        composeTestRule.onNodeWithText("Isaac Asimov").assertIsDisplayed()
        composeTestRule.onNodeWithText("1939").assertIsDisplayed()
        composeTestRule.onNodeWithText(bookDescription).assertIsDisplayed()
        composeTestRule.onNodeWithTag(R.drawable.book_placeholder.toString()).assertIsNotDisplayed()
        composeTestRule.onNodeWithTag(thumbnailString).assertIsDisplayed()
    }


    @Test
    fun testBookDetailContentWithNullValues() {
        composeTestRule.setContent {
            BookDetailContent(
                name = "I, Robot",
                authors = "Isaac Asimov",
                bookDescription = null,
                thumbnail = null,
                releaseDate = null
            )
        }

        composeTestRule.onNodeWithText("I, Robot").assertIsDisplayed()
        composeTestRule.onNodeWithText("Isaac Asimov").assertIsDisplayed()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.no_description_placeholder)).assertIsDisplayed()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.release_date_placeholder)).assertIsDisplayed()
        composeTestRule.onNodeWithTag(R.drawable.book_placeholder.toString()).assertIsDisplayed()
    }
}