package com.example.searchbooksbyauthor.ui.theme.detail

import androidx.activity.ComponentActivity
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.onRoot
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.printToLog
import com.example.searchbooksbyauthor.R
import org.junit.Rule
import org.junit.Test

class BookDetailPageTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Test
    fun testBookDetailPageStoreUrlAvailable() {
        val activity = composeTestRule.activity
        composeTestRule.setContent {
            BookDetailPage(
                authors = arrayListOf("Isaac Asimov"),
                bookName = "I, Robot",
                bookDescription = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                thumbnail = "http://books.google.com/books/content?id=_ojXNuzgHRcC&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE73ofWeWPkb4BIqYBiZvFA8kMLiSF2BkOMfHtjJLEulQnah3QKpVpwcYxjydjAYEhlhBzTyc_7jr8_varWz8ftBqbQj4KkVEWdtVB-skF_Xruow-_hONQfI2LJp0YdFo4gDUY7X8&source=gbs_api",
                releaseDate = "1939",
                storeUrl = "http://books.google.cz/books?id=OSk9zwEACAAJ&dq=inauthor:asimov&hl=&source=gbs_api",
                onBackButtonClick = {}
            )
        }

        // Perform assertions or interactions with the UI elements using the composeTestRule\
        val googlePlayIcon = composeTestRule.onNodeWithContentDescription(activity.getString(R.string.google_play_link))
        googlePlayIcon.assertExists()
        googlePlayIcon.performClick()

        // Don't know how to check if browser was opened

    }

    @Test
    fun testBookDetailPageNoStoreUrl() {
        val activity = composeTestRule.activity
        composeTestRule.setContent {
            BookDetailPage(
                authors = arrayListOf("Isaac Asimov"),
                bookName = "I, Robot",
                bookDescription = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                thumbnail = "http://books.google.com/books/content?id=_ojXNuzgHRcC&printsec=frontcover&img=1&zoom=1&edge=curl&imgtk=AFLRE73ofWeWPkb4BIqYBiZvFA8kMLiSF2BkOMfHtjJLEulQnah3QKpVpwcYxjydjAYEhlhBzTyc_7jr8_varWz8ftBqbQj4KkVEWdtVB-skF_Xruow-_hONQfI2LJp0YdFo4gDUY7X8&source=gbs_api",
                releaseDate = "1939",
                storeUrl = null,
                onBackButtonClick = {}
            )
        }
        composeTestRule.onRoot().printToLog("noStoreUrl")
        // Perform assertions or interactions with the UI elements using the composeTestRule\
        val googlePlayIcon = composeTestRule.onNodeWithContentDescription(activity.getString(R.string.google_play_link))
        googlePlayIcon.assertExists()
        googlePlayIcon.performClick()

        composeTestRule.onNodeWithText(activity.getString(R.string.no_store_url)).assertIsDisplayed()
    }
}